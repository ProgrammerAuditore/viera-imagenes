import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter layouts',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter layouts | Base de datos'),
        ),
        body: Center(
          child: Container(
            child: ListView(
              children: contenido(),
            ),
          ),
        ),
      ),
    );
  }
}

List<Widget> contenido(){
  return <Widget>[
      Image.asset(
        'images/bkg1.jpg',
        // color: Colors.black,
        // colorBlendMode: BlendMode.colorBurn,
      ),
      Container(
        padding: EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Container(
                    child: Text('MYSQL'),
                  )
                ],
              )
            ),
            Icon(
              Icons.star,
              color: Colors.red,
              size: 20.0,
              textDirection: TextDirection.ltr,
              semanticLabel: 'Icon'
            ),
          ],
        ),
      ),
      Container(
        padding: EdgeInsets.all(32),
        child: textoMysql(),
      ),

      // *** ORACLE
      Image.asset(
        'images/bkg3.jpg',
        // color: Colors.black,
        // colorBlendMode: BlendMode.colorBurn,
      ),
      Container(
        padding: EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Container(
                    child: Text('ORACLE'),
                  )
                ],
              )
            ),
            Icon(
              Icons.star,
              color: Colors.red,
              size: 20.0,
              textDirection: TextDirection.ltr,
              semanticLabel: 'Icon'
            ),
          ],
        ),
      ),
      Container(
        padding: EdgeInsets.all(32),
        child: textoOracle(),
      ),

      // ** MONGO DB
      Image.asset(
        'images/bkg2.jpg',
        // color: Colors.black,
        // colorBlendMode: BlendMode.colorBurn,
      ),
      Container(
        padding: EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Container(
                    child: Text('MONGO DB'),
                  )
                ],
              )
            ),
            Icon(
              Icons.star,
              color: Colors.red,
              size: 20.0,
              textDirection: TextDirection.ltr,
              semanticLabel: 'Icon'
            ),
          ],
        ),
      ),
      Container(
        padding: EdgeInsets.all(32),
        child: textoMongoDb(),
      )
  ];
}

Text textoMysql() {
  return Text(
    'MySQL es un sistema de gestión de bases de datos relacional desarrollado bajo licencia dual: Licencia pública general/Licencia comercial por Oracle Corporation y está considerada como la base de datos.',
    textAlign: TextAlign.justify,
  );
}

Text textoOracle() {
  return Text(
    'Oracle Database es un sistema de gestión de base de datos de tipo objeto-relacional, desarrollado por Oracle Corporation, la empresa estadounidense de hardware y software. Este tipo de sistema mejora la gestión de grandes bases de datos y también aumenta el nivel de seguridad. ',
    textAlign: TextAlign.justify,
  );
}

Text textoMongoDb() {
  return Text(
    'MongoDB es un sistema de base de datos NoSQL, orientado a documentos y de código abierto. En lugar de guardar los datos en tablas, tal y como se hace en las bases de datos relacionales, MongoDB guarda estructuras de datos BSON (una especificación similar a JSON) con un esquema dinámico, haciendo que la integración de los datos en ciertas aplicaciones sea más fácil y rápida.',
    textAlign: TextAlign.justify,
  );
}

void Test(){

}